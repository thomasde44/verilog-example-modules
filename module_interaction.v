`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2023 10:44:23 AM
// Design Name: 
// Module Name: vision
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define DATA_WIDTH 8
`define DATA_SIZE 10
module sender(input clock, reset);
    reg [`DATA_WIDTH-1:0] image [0:`DATA_SIZE-1];
    integer i;
    
    always @(posedge clock) begin
        if (reset) begin
            for (i=0; i<`DATA_SIZE; i=i+1) begin
                image[i] <= $random % 256;
            end
        end
     end
    receiver RECEIVER (
        .clock(clock),
        .reset(reset)
    );
endmodule
module receiver(input clock, reset);
    reg [`DATA_WIDTH-1:0] image [0:`DATA_SIZE-1];
endmodule

module third_module(input clock, reset);
    sender SENDER (clock, reset);
    receiver RECEIVER (clock, reset);
    
    integer i;
    always @ (posedge clock) begin
        for (i=0; i<=`DATA_SIZE-1; i=i+1) begin
            RECEIVER.image[i] <= SENDER.image[i];
        end
    end
endmodule
