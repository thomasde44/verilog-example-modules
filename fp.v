`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/03/2023 02:18:59 PM
// Design Name: 
// Module Name: fp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fp # (parameter DATA_WIDTH = 16,
   STACK_DEPTH = 100) (
   input clock, reset,
   input signed [DATA_WIDTH-1:0] rho, phiu, phid,
   output reg [1:0] eta, pushpop
   );
   parameter mu = 1, muu = 2, mud = 2;
   reg [DATA_WIDTH-1:0] phi;
   integer epsilon = 1;
   always @ (posedge clock) begin
      pushpop = 0;
      if (phiu + muu - phid - mud > epsilon) begin
         phi <= phid + mud;
         eta <= 2'b11;
      end
      else if (phiu + muu - phid - mud > epsilon) begin
         phi <= phid + mud;
         eta <= 2'b01;
      end
      else begin
         phi <= phi + mu;
         eta <= 2'b00;
      end
   end
endmodule
